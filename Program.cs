﻿using StudentManagementException;

StudentManagement studentManagement = new StudentManagement();
Student student = new Student();
int choice = 0;

do
{
    try
    {
        studentManagement.Menu();
        choice = int.Parse(Console.ReadLine());
        Console.Clear();

        switch (choice)
        {
            case 1:
                studentManagement.View(student);
                break;
            case 2:
                Console.WriteLine("Nhap ten hoc sinh: ");
                student.Name = Console.ReadLine();
                Console.WriteLine("Nhap tuoi hoc sinh: ");
                student.Age = int.Parse(Console.ReadLine());
                Console.WriteLine("Nhap GPA cua hoc sinh: ");
                student.GPA = double.Parse(Console.ReadLine());

                studentManagement.StudentGrade(student);
                break;
            case 3:
                Console.WriteLine("Hen gap lai");
                Environment.Exit(0);
                break;
            default:
                Console.WriteLine("Nhap so trong khoang bang tren");
                break;
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
} while (choice != 3);