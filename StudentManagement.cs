﻿

namespace StudentManagementException
{
    public class StudentManagement
    {
        public void StudentGrade(Student student)
        {
            try
            {
                Console.WriteLine("Xep loai: ");
                if (student.GPA >= 8.0)
                {
                    Console.WriteLine($"{student.Name} dat hoc sinh gioi");
                }
                else if (student.GPA < 8.0 && student.GPA >= 6.5)
                {
                    Console.WriteLine($"{student.Name} dat hoc sinh kha");
                }
                else if (student.GPA < 6.5 && student.GPA >= 5.0)
                {
                    Console.WriteLine($"{student.Name} dat hoc sinh trung binh");
                }
                else
                {
                    Console.WriteLine($"{student.Name} dat hoc sinh yeu");
                }
            }
            catch (FormatException)
            {
                Console.WriteLine("Nhap sai dinh dang GPA, vui long nhap lai");
            }
        }

        public void View(Student student)
        {
            Console.WriteLine("Ten hoc sinh: " + student.Name);
            Console.WriteLine("Tuoi hoc sinh: " + student.Age);
            Console.WriteLine("Diem GPA: " + student.GPA);
        }

        public void Menu()
        {
            Console.WriteLine("---Quan ly hoc sinh---");
            Console.WriteLine("1. Hien thi thong tin hoc sinh.");
            Console.WriteLine("2. Xet diem va nhap thong tin hoc sinh.");
            Console.WriteLine("3. Exit.");
            Console.WriteLine("Nhap lua chon: ");
        }
    }
}
